package Projekt_Convert;
import java.util.Scanner;
public class ConvertAmountWithLoop {
    static final double ROUBLES_PER_DOLLAR = 72.12; // курс покупки доллара

    public static void main(String[] args) {

        int dollars;
        double roubles;
        int digit, i, n;

        Scanner input = new Scanner(System.in);
        // отобразить инструцию
        instruct();
        // Получать количество конвертаций до тех пор,
        // пока не введено корректное значение
        do {
            System.out.print("Введите корректное количество конвертаций: ");
            n = input.nextInt();
        } while (n <= 0);
        // До тех пор пока не конвертированны все суммы, получать,
        // отображать и конвертировать суммы денег в американских
        // долларах и отображать суммы денег в российских рублях
        for (i = 0; i < n; ++i) {
            // Получить сумму в долларах
            System.out.print("Введите сумму денег в американских долларах: ");
            dollars = input.nextInt();

            System.out.print(dollars);
            if (5 <= dollars && dollars <= 20)
                System.out.print(" американских долларов равны ");
            else {
                digit = dollars % 10;
                if (digit == 1)
                    System.out.print(" американский доллар равен ");
                else if (2 <= dollars && dollars <= 4)
                    System.out.print(" американских доллара равны ");
                else
                    System.out.print(" американских долларов равны ");
            }
            // Вычислить сумму в рублях
            roubles = find_roubles(dollars);

            // Отобразить сумму денег в российских рублях
            System.out.println((int) (roubles * 100) / 100.0
                    + " российского рубля");
        }
    }

        // отображает инструцию
        public static void instruct () {
            System.out.print("Эта программа конвертирует сумму денег"
                    + "из американских долларов в российские рубли.");
            System.out.print("Курс покупки равен " + ROUBLES_PER_DOLLAR
                    + "рубля.\n");
        }

        // Конвертирует сумму денег из американских долларов в российские рубли
        public static double find_roubles(int dollars) {
        return ROUBLES_PER_DOLLAR * dollars;
        }
}

