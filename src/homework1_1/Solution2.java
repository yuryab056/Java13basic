package homework1_1;

    import java.util.Scanner;

    public class Solution2 {
        public static void main(String[] args) {
            double a, b, c, s;

            Scanner input = new Scanner(System.in);

            a = input.nextDouble();
            b = input.nextDouble();

            if (a >= 1 && b <= 99) {
                c = (Math.pow(a,2) + Math.pow(b,2))/2;
                s = Math.sqrt(c);
                System.out.println(+ s);

            } else {
                System.out.println("");
            }
        }
    }

