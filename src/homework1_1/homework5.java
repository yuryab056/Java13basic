package homework1_1;

import java.util.Scanner;

public class homework5 {
    public static void main(String[] args) {
        double inches, centimeters;

        Scanner scanner = new Scanner(System.in);

        // Получить данные в дюймах
        System.out.print("Введите число в дюймах от  от 1 до 999: ");
        inches = scanner.nextDouble();
        if (inches > 0 && inches < 1000) {

            // Перевести дюймы в сантиметры
            centimeters = inches * 2.54;

            // Отобразить получившиеся сантиметры
            System.out.println(inches + " дм равно: " + centimeters + " сm ");
        }else
        {
            System.out.println("не корректные данные");
        }
    }
}
