package homework1_1;

import java.util.Scanner;

public class homework6 {

    public static void main(String[] args) {
        final double ONE_MILE = 1.60934;
        double kilometers, miles;

        Scanner scanner = new Scanner(System.in);

        // Получить данные
        System.out.print("Введите число в километрах от  от 1 до 999: ");
        kilometers = scanner.nextDouble();
        if (kilometers > 0 && kilometers < 1000) {

            // Перевести
            miles = kilometers / ONE_MILE;

            // Отобразить получившиеся данные
            System.out.println(kilometers + " км равно: " + miles + " мл ");
        }else
        {
            System.out.println("не корректные данные");
        }
    }
}

