package homework1_2;

import java.util.Scanner;

public class homework10 {

    public static void main(String[] args) {
        final double E;
        double n;

        Scanner input = new Scanner(System.in);

        n = input.nextDouble();

        if (n > -500 && n < 500) {
            System.out.println(Math.log(Math.pow(Math.E, n)) == n);
        } else
        {
            System.out.println(n);
        }
    }
}


