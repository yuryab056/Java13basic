package homework1_2;

import java.util.Scanner;

public class homework11 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        int sum;

        if (a > b && a > c) {
            sum = a - b - c;
        } else if (b > a && b > c){
            sum = b - a - c;
        } else {
            sum = c - a - b;
        }
        System.out.println(sum != 0 && sum < 0);
    }
}