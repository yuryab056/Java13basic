package homework1_2;

import java.util.Scanner;

public class homework5 {
    public static void main(String[] args) {
        double a, b, c, d;

        Scanner input = new Scanner(System.in);

        a = input.nextInt();
        b = input.nextInt();
        c = input.nextInt();

        if ((a > -100 && a < 100) && (b > -100 && b < 100) && (c > -100 && c < 100)) {

            d = (Math.pow(b,2) - (4 * a * c));
            if (d >= 0){
                System.out.println("Решение есть");
            } else{
                System.out.println("Решения нет");
            }
        }
    }
}
