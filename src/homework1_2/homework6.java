package homework1_2;

import java.util.Scanner;

public class homework6 {
    public static void main(String[] args) {
        int a, d;

        Scanner input = new Scanner(System.in);

        a = input.nextInt();

        if (a >= 0 && a < 10000) {

            d = a / 500;

            if (d < 1) {
                System.out.println("beginner");
            }
            if (d >= 1 && d < 3) {
                System.out.println("pre-intermediate");
            }
            if (d >= 3 && d < 5) {
                System.out.println("ntermediate");
            }
            if (d >= 5 && d < 7) {
                System.out.println("upper-intermediate");
            }
            if (d >= 7) {
                System.out.println("fluent");
            } else {
                System.out.println("");
            }
        }
    }
}
