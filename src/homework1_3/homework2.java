package homework1_3;

import java.util.Scanner;

public class homework2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        int res = 0;
        for (int i = m; i <= n; i++) {
            res += i;
        }
        System.out.println(res);
    }
}