package homework1_3;

import java.util.Scanner;

public class homework3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        int res = 0;
        for (int i = 1; i <= n; i++) {
            res += Math.pow(m,i); //res = res * i
            System.out.println("Промежуточный результат: " + res);
        }
        System.out.println("Итоговый результат: " + res);

        //респект Роману!
        //так тоже работает
//        int res1 = 1;
//        for (int i = 1; i <= n; i++, res1 *= i) {
//            System.out.println(res1);
//        }

    }
}
