package homework1_3;

import java.util.Scanner;

/*.
Даны положительные натуральные числа m и n. Найти остаток от деления m на
n, не выполняя операцию взятия остатка.

Ограничения:
0 < m, n < 10
*/
public class homework5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int res;

        if (m > n && n != 1) {
            res = m / n;
            System.out.println(res);
        } else if (m < n) {
            System.out.println(m);
        }
        else  {
            System.out.println(0);
        }
    }
}

