package homework1_3;
/*      В банкомате остались только купюры номиналом 8 4 2 1. Дано положительное
        число n - количество денег для размена. Необходимо найти минимальное
        количество купюр с помощью которых можно разменять это количество денег
        (соблюсти порядок: первым числом вывести количество купюр номиналом 8,
        вторым - 4 и т д)

        Ограничения:
        0 < n < 1000000
*/
import java.util.Scanner;

public class homework6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        if (n % 8 >= 0){
            System.out.print(n / 8 + " ");
        n = n % 8;
    }
        if (n % 4 >= 0){
            System.out.print(n / 4 + " ");
            n = n % 4;
        }
        if (n % 2 >= 0){
            System.out.print(n / 2 + " ");
            n = n % 2;
        }
        if (n % 1 >= 0){
            System.out.print(n / 1);
            n = n % 1;
        }
    }
}