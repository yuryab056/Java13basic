package homework1_3;
import java.util.Scanner;
/*
На вход подается:
○ целое число n,
○ целое число p
○ целые числа a1, a2 , … an
Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго
больше p.

Ограничения:
0 < m, n, ai < 1000
  */
public class homework8 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner("6\n" +
                "29\n" +
                "40 37 97 72 80 18");
        int n = scanner.nextInt(), p = scanner.nextInt();
        int sum = 0;
        int countIteration = 0;
        for (int i = scanner.nextInt(); i > p;
             i = n > ++countIteration ? scanner.nextInt() : 0)
            sum += i;
        System.out.println(sum);
    }
}
