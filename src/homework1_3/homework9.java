package homework1_3;
/*
На вход последовательно подается возрастающая последовательность из n
целых чисел, которая может начинаться с отрицательного числа.

Посчитать и вывести на экран, какое количество отрицательных чисел было
введено в начале последовательности. Помимо этого нужно прекратить
выполнение цикла при получении первого неотрицательного числа на вход.

Ограничения:
0 < n < 1000
-1000 < ai < 1000

 */
import java.util.Scanner;

public class homework9 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int count = 0;
        for (int i = scanner.nextInt(); i < 0; i = scanner.nextInt()) {
            count++;
        }
        System.out.println(+ count);
    }
}
